function RealBoard(size, mineNum){

  var actualMineCount = 0;
  var mine = new Array();
  var board = new Array();

  for (var i = 0; i < size[0]; i++){
    mine[i] = new Array();
    board[i] = new Array();
  }

  this.init = function(){

    for (var i = 0; i < size[0]; i++){
      for (var j = 0; j < size[1]; j++){
        var temp = random();
        if(temp > 0.99 && actualMineCount < mineNum && (!mine[i][j])){

          mine[i][j] = true;
          actualMineCount++;
        }
        else if(!mine[i][j]){
          mine[i][j] = false;
        }
      }
    }
    if(actualMineCount < mineNum){
      this.init();
    }
  }

  this.storeRealBoard = function(){
    for (var x = 0; x < size[0]; x++){
      for (var y = 0; y < size[1]; y++){
        if(this.isMine(x,y)){
            board[x][y] = "\u2605";
        }
        else{
          if(this.mineCountNearby(x, y) > 0){
            board[x][y] = this.mineCountNearby(x,y);
          }
        }
      }
    }
  }

  this.isMine = function(x, y){
    return mine[x][y];
  }

  this.isMineVal = function(x, y){
    var val = 0;
    if (x >= 0 && y >= 0 && x < size[0] && y < size[1] && mine[x][y]){
          val = 1;
    }

    return val;

  }

  this.getBoardVals = function () {
    return board;
  }

  this.mineCountNearby = function(x, y){
    var count = 0;

    count += this.isMineVal(x - 1, y -1);
    count += this.isMineVal(x, y -1);
    count += this.isMineVal(x + 1, y -1);
    count += this.isMineVal(x - 1, y);
    count += this.isMineVal(x + 1, y);
    count += this.isMineVal(x - 1, y + 1);
    count += this.isMineVal(x, y + 1);
    count += this.isMineVal(x + 1, y + 1);

    return count;
  }

  this.getMines = function(){
    return mine;
  }

  this.drawUnderlay = function() {

    for (var x = 0; x < mine.length; x++){
      for (var y = 0; y < mine[x].length; y++){
        if(this.isMine(x,y)){
          fill(0);
          rect(x * rectSize  + rectGap, y * rectSize + rectGap, rectSize - rectGap, rectSize - rectGap, rectFillet);
        }
        if (board[x][y] != null){
          fill(255);
          writeText(board[x][y], x, y);
        }
      }
    }
  }

  this.toString = function(){
    for (var x = 0; x < mine.length; x++){
      for (var y = 0; y < mine[x].length; y++){
        console.log(x + " " + y + " " + mine[x][y]);
      }
    }
        console.log("actual " + actualMineCount);
  }


}
