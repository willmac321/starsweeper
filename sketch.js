let rectSize = 50;
let rectFillet = 5;
let rectGap = 1;
let size = [30, 16];
let mineNum = 99;
let textSi = 25;
var realBoard;
var userBoard;
var okToPlay = false;
var passOnce = false;
var gap = 30;
var xSize, ySize, mineSize, button;

function setup() {
  var cnv = createCanvas(windowWidth, windowHeight);
  background(200);

  var x = (windowWidth - width) / 2;
  var y = (windowHeight - height) / 2;

  cnv.position(x, y);
  createWelcomeScreen();

}

function draw() {
  if(okToPlay && !userBoard.isEndGame() ){
            background(56);
    if (mineNum < size[0] * size[1]){
          realBoard.drawUnderlay();
          userBoard.drawOverlay();
    }
  }
}

function letsPlay(){
  if (mineNum < size[0] * size[1]){


//    checkXSize(size[0]);
    checkYSize(size[1]);

    var cnv = createCanvas(size[0]*rectSize + 1, size[1]*rectSize + 1);
    var x = (windowWidth - width) / 2;
    var y = (windowHeight - height) / 2;

    cnv.position(x, y);
    button.hide();
    xSize.hide();
    ySize.hide();
    mineSize.hide();

    realBoard = new RealBoard(size, mineNum);
    userBoard = new UserBoard(size);
    realBoard.init();
    realBoard.storeRealBoard();
    userBoard.init(realBoard.getBoardVals());
        okToPlay = true;
  }
  else{
    textSize(textSi);
    text("Select # of Mines in bounds of Size", width/2, height/2);
  }
}

function myMineInputEvent() {
  mineNum = this.value();
}

function myYInputEvent() {
  checkYSize(this.value());
}

function myXInputEvent() {
  checkXSize(this.value());
}

function checkXSize(x){
  if((x * rectSize >= windowWidth) && (x * rectSize < windowHeight)){
    textSi = textSi * floor(floor(windowWidth/x) / rectSize);
    rectSize = floor(windowWidth/x);

  }else if ((x * rectSize >= windowHeight)){
    textSi = textSi * floor(floor(windowHeight/x) / rectSize);
    rectSize = floor(windowHeight/x);
  }
  else if(x * rectSize >= windowWidth)
  {
    textSi = textSi * floor(floor(windowWidth/x) / rectSize);
    rectSize = floor(windowWidth/x);
  }
  size[0] = x;
}
function checkYSize(y){
  if((y * rectSize >= windowHeight) && (y * rectSize < windowWidth)){
    textSi = textSi * floor(floor(windowHeight/y) / rectSize);
    rectSize = floor(windowHeight/y);

  }else if ((y * rectSize >= windowWidth)){
    textSi = textSi * floor(floor(windowWidth/y) / rectSize);
    rectSize = floor(windowWidth/y);
  }
  else if(y * rectSize >= windowHeight)
  {
    textSi = textSi * floor(floor(windowHeight/y) / rectSize);
    rectSize = floor(windowHeight/y);
  }
  size[1] = y;
}

function writeText(t, x, y){
  textSize(textSi);
  textAlign(CENTER, CENTER);
  text(t, x * rectSize + rectSize/2, y * rectSize + rectSize/2);
}

function mousePressed(){
  if(okToPlay && passOnce){
    if (mouseIsPressed && (mouseButton == LEFT) && !keyIsPressed){
      userBoard.mClicked(mouseX/rectSize, mouseY/rectSize, mouseIsPressed);
      userBoard.checkLossCondition();
      userBoard.checkWinCondition();
    }else if (mouseIsPressed && (mouseButton == LEFT) && keyIsPressed ==true && keyCode == CONTROL){
      userBoard.ctrlClicked(mouseX/rectSize, mouseY/rectSize, mouseIsPressed);
    }
  }
  else if(okToPlay){
    passOnce = true;
  }

}

  function createWelcomeScreen() {
    textAlign(CENTER, CENTER);
    xSize = createInput(size[0], 'number');
    ySize = createInput(size[1], 'number');
    mineSize = createInput(mineNum, 'number');
    button = createButton('Play');
    textSize(textSi)

    var x = windowWidth/2;
    var y = windowHeight/6;


    s = "StarSweeper"
    text(s, x, y);

    y += gap + textSi/2;
    s = "X Spaces: "
    textSize(14);
    text(s, x - textWidth(s)/2, y);
    xSize.position(x, y - xSize.height/2);
    y += gap + 14/2;
    s = "Y Spaces: ";
    text(s, x - textWidth(s)/2, y);
    ySize.position(x, y - ySize.height/2);
    y += gap + 14/2;
    s = "Mine Amount: " ;
    text(s, x - textWidth(s)/2, y);
    mineSize.position(x, y - mineSize.height/2);
    y += gap + 14/2;
    text("Left click to uncover, ctrl + click to flag or mark cells.", x, y);
    y += gap + 14/2;
    button.position(x, y);

    xSize.input(myXInputEvent);
    ySize.input(myYInputEvent);
    mineSize.input(myMineInputEvent);
    button.mousePressed(letsPlay);
}
