function UserBoard(size){

  var flag = new Array(size[0]);
  var question = new Array(size[0]);
  var clicked = new Array(size[0]);
  var values = new Array(size[0]);
  var flagCount;
  var uncoveredCount;
  var endGameFlag = false;
  var lostCoord = new Array(2);

  for (var i = 0; i < size[0]; i++){
    flag[i] = new Array(size[1]);
    question[i] = new Array(size[1]);
    clicked[i] = new Array(size[1]);
    values[i] = new Array(size[1]);
  }

  this.init = function(cellValues){
    flagCount = 0;
    uncoveredCount = 0;
    for (var i = 0; i < size[0]; i++){
      for (var j = 0; j < size[1]; j++){
        flag[i][j] = false;
        question[i][j] = false;
        clicked[i][j] = false;
        values[i][j] = cellValues[i][j];
      }
    }
  }

  this.uncoverBoard = function(){
    if (!this.isEndGame()){
      fill(56);
      rect(0, 0 , size[0]*rectSize+1, size[1]*rectSize+1 , rectFillet);
      fill(0);
      for (var i = 0; i < size[0]; i++){
        for (var j = 0; j < size[1]; j++){
          clicked[i][j] = true;
        }
      }

      realBoard.drawUnderlay();
      this.drawOverlay();

      fill(255, 0, 0);
      writeText("X", lostCoord[0], lostCoord[1]);
    }
    endGameFlag = true;
  }

  this.checkWinCondition = function() {
    //console.log(uncoveredCount);
    var test = false;
    for (var i = 0; i < size[0]; i++){
      for (var j = 0; j < size[1]; j++){
        if((uncoveredCount === (size[0]*size[1] - mineNum)) && !this.isEndGame()){
          textSize(textSi);
          fill(255,0,0);
          text(":)  Winner! :)", size[0]*rectSize/2, size[1]*rectSize/2);
          fill(0);
        }
      }
    }
    return test;
  }

  this.checkLossCondition = function() {
    var test = false;
    for (var i = 0; i < size[0]; i++){
      for (var j = 0; j < size[1]; j++){
        if(clicked[i][j] && realBoard.isMine(i, j) && !test){
          lostCoord[0] = i;
          lostCoord[1] = j;
          this.uncoverBoard();
          test = true;
        }
      }
    }
    return test;
  }

  this.ctrlClicked = function(x, y, mouseClicked){
    if (x >= 0 && y >= 0 && x < size[0] && y < size[1]){
      for (var i = 0; i < size[0]; i++){
        for (var j = 0; j < size[1]; j++){
          var xT = x;
          var yT = y;
          var firstTime = (xT > i && xT < i + 1 && yT > j && yT < j + 1 && mouseClicked);
          if(firstTime && !question[i][j] && !flag[i][j] && !clicked[i][j]){
            flag[i][j] = false;
            question[i][j] = true;
            j = size[1] + 1;
            i = size[0] + 1;
          }else if(firstTime && !flag[i][j] && question[i][j] && flagCount < mineNum && !clicked[i][j]){
            question[i][j] = false;
            flag[i][j] = true;
            flagCount++;
            j = size[1] + 1;
            i = size[0] + 1;
          }else if (firstTime && flag[i][j] && !question[i][j] && !clicked[i][j]){
            question[i][j] = false;
            flag[i][j] = false;
            flagCount--;
            j = size[1] + 1;
            i = size[0] + 1;
          }
          else if(firstTime && question[i][j] && !clicked[i][j]){
            question[i][j] = false;
            j = size[1] + 1;
            i = size[0] + 1;
          }
        }
      }
    }
  }

  // function that executes for each cell object
  this.mClicked = function(x, y, mouseClicked = false, v = null) {
    var val = v;
    if (x >= 0 && y >= 0 && x < size[0] && y < size[1]){
      for (var i = 0; i < size[0]; i++){
        for (var j = 0; j < size[1]; j++){
          var xT = x;
          var yT = y;
          var firstTime = (xT > i && xT < i + 1 && yT > j && yT < j + 1 && mouseClicked);
          var anotherTime = (xT === i && yT === j);
          var isFlagged = question[i][j] || flag[i][j];
          if((firstTime || anotherTime) && !isFlagged){
            if(firstTime){
              val = values[i][j];
            }
            if ((mouseClicked || anotherTime) && !clicked[i][j] && values[i][j] === val && !isFlagged){
              clicked[i][j] = true;
              uncoveredCount++;
              this.mClicked(i, j - 1, values[i][j]);
              this.mClicked(i - 1, j, values[i][j]);
              this.mClicked(i + 1, j, values[i][j]);
              this.mClicked(i, j + 1, values[i][j]);
            }else if(values[i][j] == null && (mouseClicked || anotherTime) && !clicked[i][j] && !isFlagged){
              clicked[i][j] = true;
              uncoveredCount++;
              //console.log("part1 " + xT + " " + yT + " " + anotherTime);
              this.mClicked(i - 1, j - 1);
              this.mClicked(i, j - 1);
              this.mClicked(i + 1, j - 1);
              this.mClicked(i - 1, j);
              this.mClicked(i + 1, j);
              this.mClicked(i - 1, j + 1);
              this.mClicked(i, j + 1);
              this.mClicked(i + 1, j + 1);
            }
            else if(values[i][j] !="\u2605" && !mouseClicked && anotherTime && !isFlagged && !clicked[i][j]){
              clicked[i][j] = true;
              uncoveredCount++;
            }
          }
        }
      }
    }
  }

  this.drawShapes = function(arr, str){
    for (var x = 0; x < arr.length; x++){
      for (var y = 0; y < arr[x].length; y++){
        switch(str){
          case "clicked":
            if (!arr[x][y]){
              rect(x * rectSize + rectGap, y * rectSize + rectGap, rectSize - rectGap, rectSize - rectGap, rectFillet);
            }
            break;
          case "flag":
            fill(255, 0, 0);
            if (arr[x][y]){
              writeText("\u2691", x, y);
            }
            fill(255);
            break;
          case "question":
            fill(0);
            if (arr[x][y]){writeText("?", x, y);}
            fill(255);
        }
      }
    }
  }

  this.drawOverlay = function(){
    this.drawShapes(clicked, "clicked");
    this.drawShapes(flag, "flag");
    this.drawShapes(question, "question");

  }

  this.isEndGame = function(){
    return endGameFlag;
  }

  this.toString = function(){
    for (var x = 0; x < values.length; x++){
      for (var y = 0; y < values[x].length; y++){

        console.log(x + " " + y + " " + clicked[x][y]);
      }
    }
  }
}
